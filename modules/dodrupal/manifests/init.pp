class dodrupal (

  # class arguments
  # ---------------
  # setup defaults

  $user = 'web',

  # end of class arguments
  # ----------------------
  # begin class

) {

  # don't install Console_Table with PEAR because drush can't see it
  #pear::package { "Console_Table": }

  # install drush
  pear::package { 'drush':
    repository => "pear.drush.org",
    require => Pear::Package['PEAR'],
  }

  # use first run as root to get dependencies
  exec { 'drush-first-run' :
    path => '/usr/bin:/bin:/usr/local/zend/bin/',
    command => 'drush --version',
    user => 'root',
    require => Pear::Package['drush'],
  }->
  # then clean up the cache directory permissions to anyone can run it
  exec { 'drush-fix-cache-perms' :
    path => '/usr/bin:/bin',
    user => 'root',
    command => 'chmod -R 777 /tmp/drush',
  }
}
