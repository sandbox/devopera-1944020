class dodrupal::base (

  # class arguments
  # ---------------
  # setup defaults

  $user = 'web',
  $user_password = 'admLn**',
  $group = 'www-data',

  # database connection values
  $db_type = 'mysql',
  $db_name = 'dodrupal',
  $db_user = 'dodrupal',
  $db_pass = 'admLn**',
  $db_host = 'localhost',
  $db_port = '3306',
  $db_grants = ['all'],

  # end of class arguments
  # ----------------------
  # begin class

) {
  dodrupal::drush { 'install-drupal':
    command => 'dl drupal-7 -v -d -q --drupal-project-rename="drupal-7"',
    cwd => '/var/www/html/',
    cwd_check => false,
    user => $user,
    group => $group,
    require => Docommon::Stickydir['/var/www/html'],
    creates => '/var/www/html/drupal-7/.htaccess',
  }->

  # create symlink from our home folder
  file { "/home/${user}/drupal-7":
    ensure => 'link',
    target => '/var/www/html/drupal-7',
  }->

  # use installapp macros to install repo, hosts and vhosts
  dorepos::installapp { 'appconfig-drupal' :
    repo => {
      provider => 'git',
      path => '/var/www/git/github.com',
      source => 'git://github.com/devopera/appconfig-drupal.git',
    },
    byrepo_filewriteable => {
      'filewriteable-dodrupal' => { 
        filename => '/var/www/html/drupal-7/sites/default/files'
      },
    },
    require => [File['/var/www/git/github.com'], Dodrupal::Drush['install-drupal']],
  }

  # create a mysql database for Drupal, then install a fresh DB and setup the admin user
  $db_url = "${db_type}://${db_user}:${db_pass}@${db_host}:${db_port}/${db_name}"
  mysql::db { "${db_name}":
    user     => $db_user,
    password => $db_pass,
    host     => $db_host,
    grant    => $db_grants,
  }->
  dodrupal::drush { 'install site and admin user':
    command =>
      "site-install --yes  --site-name=\"Devopera Drupal Demo\" --db-url=${db_url} --account-name=admin --account-pass=${db_pass}",
    cwd => '/var/www/html/drupal-7',
    user => $user,
    group => $group,
  }
}
